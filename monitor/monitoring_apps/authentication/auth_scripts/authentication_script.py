###############################################################################################################################################################
###############################################################################################################################################################

# coding: utf-8

import os, subprocess
import getpass
import sys

username=os.environ['USER']
if username !="engineering":
  print("Please run the script with engineering user only and have sudo access to it")
  sys.exit()

sudo_password=getpass.getpass()

if not sudo_password:
  sys.exit()
# Now, let’s set up a basic authentication using htpasswd. Follow below commands to generate the secret for credentials.
# NOTE : If required then run the below commands

apt_update_command='apt-get update'
apt_update_exitcode_status = os.system('echo "%s"|sudo -S %s' % ( sudo_password, apt_update_command))
if apt_update_exitcode_status != 0:
    print ("There was an error at %s with exit status code %s" %(apt_update_command, apt_update_exitcode_status))
    sys.exit()


update_cmd='apt-get install -y  apache2-utils'
update_cmd_exitcode_status = os.system('echo "%s" |sudo -S %s' % ( sudo_password, update_cmd))
if update_cmd_exitcode_status != 0:
    print ("There was an error at %s with exit status code %s" %(update_cmd, update_cmd_exitcode_status))
    sys.exit()


# Let’s create an auth file with username and password :

pwd_cmd='htpasswd -b -c auth monitoring tsss!@#'
pwd_cmd_exitcode_status = os.system('echo "%s" |sudo -S %s' % ( sudo_password, pwd_cmd))
if pwd_cmd_exitcode_status != 0:
    print ("There was an error at %s with exit status code %s" %(pwd_cmd, pwd_cmd_exitcode_status))
    sys.exit()


# Create k8s secret :

secret_cmd='kubectl create secret generic basic-auth09 -n default --from-file=auth'
secret_cmd_exitcode_status = os.system('echo "%s" |sudo -S %s' % ( sudo_password, secret_cmd))
if secret_cmd_exitcode_status != 0:
    print ("There was an error at %s with exit status code %s" %(secret_cmd, secret_cmd_exitcode_status))
    sys.exit()

# secret "basic-auth09" created


# Verify the secret :
#kubectl get secret basic-auth09 -n monitoring -o yaml

# Run the ingress file named ingress.yaml : Here we are creating the ingress along with adding annotations required for authentication of ingress

# /home/engineering/kubernetes/monitoring_apps/authentication
ingress_cmd='kubectl apply -f /home/engineering/kubernetes/monitoring_apps/authentication'
ingress_cmd_exitcode_status = os.system('echo "%s" |sudo -S %s' % ( sudo_password, ingress_cmd))
if ingress_cmd_exitcode_status != 0:
    print ("There was an error at %s with exit status code %s" %(ingress_cmd, ingress_cmd_exitcode_status))
    sys.exit()



# Now check the domain names whether authentication is done successfully (OR) not .


print("Authentication Apps and Ingress were installed Successfully now ...")

###############################################################################################################################################################
###############################################################################################################################################################


