###############################################################################################################################################################
###############################################################################################################################################################

# Using Kubernetes ingress controller for authenticating applications:
# ===================================================================

#!/bin/bash
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command failed with exit code $?."' EXIT

myuser=$USER
if [ $myuser != "engineering" ]; then
echo "Please run the script with engineering and must have sudo access to engineering....exiting.."
exit 1
fi
file="/home/engineering/.passwd/passwd"
if [ ! -f "$file" ]
then
   echo "Please keep the file with $myuser password in '${file}'...exiting now and try again"
       exit 1
fi

#file01="/home/engineering/.passwd/passwd01"
#if [ ! -f "$file01" ]
#then
#   echo "Please keep the file with $myuser password in '${file01}'...exiting now and try again"
#       exit 1
#fi


# Now, let’s set up a basic authentication using htpasswd. Follow below commands to generate the secret for credentials.
# NOTE : If required then run the below commands

#cd /home/engineering/kubernetes/prometheus/authentication

cd /home/engineering/kubernetes/monitoring_apps/authentication/auth_scripts

#cat $file |  sudo -S apt-get update
cat $file |  sudo -S apt-get install apache2-utils -y

# Let’s create an auth file with username and password :

htpasswd -b -c auth monitoring tsss!@#


# Create k8s secret :

kubectl create secret generic basic-auth09 -n default --from-file=auth

# secret "basic-auth09" created


# Verify the secret :

kubectl get secret basic-auth09 -n default -o yaml

# Run the ingress file named ingress.yaml : Here we are creating the ingress along with adding annotations required for authentication of ingress

#kubectl create -f ingress.yaml

# Now check the domain names whether authentication is done successfully (OR) not .


echo "Authentication apps Secret was created Successfully now ..."

cd /home/engineering/kubernetes/monitoring_apps/authentication

kubectl apply -f .

echo "Ingress & Cetificates Created For Monitoring Apps Successfully Now ..."

###############################################################################################################################################################
###############################################################################################################################################################

